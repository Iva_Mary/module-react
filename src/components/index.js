import './card.css'


function Card({url, title, description, price, weight, button = '/images/button.png', activeCount}) {

    return (
        <div className="card">
            <div className="card__info">
                <img className='card__preview' src={url} alt="" />
                <h2 className="card__title">{title}</h2>
                <p className='card__description'>{description}</p>
            </div>

            <div className="card__bottom">
                <div className="card__price">
                    {price} ₽ / {weight} г
                </div>
                <div className="card__button">
                    <button onClick={() => activeCount(price)} className='btn__sum'><img src={button} alt="" /></button>
                </div>
            </div>
        </div>
    );
}

export default Card;