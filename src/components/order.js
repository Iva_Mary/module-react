import './order.css';

function Order ({mainUrl, name, price, deleteUrl}) {
    return (
        <div className="order">
            <img src={mainUrl} alt="" className="order__img" />
            <p className="order__info">{name}</p>
            <div className="order__item">
                <p className="price">{price}</p>
                <img src={deleteUrl} alt="" className="order__delete" />
            </div>
        </div>
    )
}

export default Order;