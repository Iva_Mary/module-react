const basket = [
    {
        id: 1,
        mainUrl: '/images/oisters.png',
        name: 'Устрицы по рокфеллеровски',
        price: '2500 ₽',
        deleteUrl: '/images/delete.png',
    },
    {
        id: 2,
        mainUrl: '/images/bief.png',
        name: 'Свиные ребрышки на гриле с зеленью',
        price: '1600 ₽',
        deleteUrl: '/images/delete.png',
    },
    {
        id: 3,
        mainUrl: '/images/shrimps.png',
        name: 'Креветки по-королевски в лимонном соке',
        price: '1820 ₽',
        deleteUrl: '/images/delete.png',
    }
]

export default basket;