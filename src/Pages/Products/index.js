import './index.css';
import Card from '../../components/index';
import products from '../../products.js'
import { Outlet, Link } from 'react-router-dom';
import { useState } from 'react';

function Products() {

    const [countCards, setCountCards] = useState(0);
    const [sumPrice , setSumPrice] = useState(0);

    const activeCount = (price) => {
        setCountCards(countCards + 1);
        setSumPrice(sumPrice + price);
        console.log(price);
    }
    return (
        <div className="products">
            <div className="container">
            <header className='products__header'>
                    <div>
                        <h1 className='products__title'>наша продукция</h1>
                    </div>

                    <div className='basket'>
                        <div className='basket__text'>
                            <span className="count"> {countCards} товара</span>
                            <span className="countPrice">на сумму {sumPrice} </span>
                        </div>
                        <Link to="/basket"><img className='basket__img' src='./images/basket.png'/></Link>
                    </div>
                </header>

                <main className='products__main'>
                    {products.map(key => {
                        return (
                            <Card
                                key={key.id}
                                url={key.url}
                                title={key.name}
                                description={key.description}
                                price = {key.price}
                                weight ={key.weight}
                                activeCount={activeCount}
                            />
                        )
                    })}
                </main>
            </div>

        </div>
    );
}

export default Products; 