import './basket.css';
import Order from '../../components/order';
import basket from '../../basket';
import { Outlet, Link } from 'react-router-dom';

function Basket () {
    return (
        <div className="basket">
            <div className="container">

                <header className="basket__header">
                    <div className="header__btn">
                        <Link to="/products"><img src="/images/back.png" alt="" /></Link>
                    </div>
                    <h1 className="basket__title">КОРЗИНА С ВЫБРАННЫМИ ТОВАРАМИ</h1>
                </header>

                <main className="basket__main">
                    <div className="basket__items">
                        {basket.map(key => {
                            return (
                                <Order
                                    key={key.id}
                                    mainUrl={key.mainUrl}
                                    name={key.name}
                                    price={key.price}
                                    deleteUrl={key.deleteUrl}
                                />
                            )
                        })}
                    </div>
                </main>

                <footer className='footer'>

                    <hr className='line'/>

                    <div className="footer__items container">
                        <p className="footer__amount">Заказ на сумму: <span className='amount__item'>6220 ₽</span></p>
                        <button className="footer__btn">Оформить заказ</button>
                    </div>
                </footer>
            </div>
        </div>
    )
}

export default Basket;