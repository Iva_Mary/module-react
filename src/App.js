import './reset.css';
import './App.css'
import Products from "./Pages/Products/index.js";
import Basket from './Pages/Basket/basket.js';
import { Outlet, Link } from 'react-router-dom';

// import { Outlet } from 'react-router-dom';

function App() {
  return (

    <div className="App">
      {/* <header className='app__header'>
        <Link to="/products" className='link__text'>Главная</Link>
        {/* <br/> */}
        {/* <Link to="/basket" className='link__text'>Корзина</Link>
      </header> */}

      <main>
        {/* <Products />
        <Basket /> */}
        <Outlet />
      </main>
    </div>
  );
};
export default App;
